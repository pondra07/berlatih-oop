<?php

    class animal{
        public $name;
        public $legs = 4;
        public $cold_blooded = "no";
        public $jump = "Hop Hop";
        public $yell = "Auooo";

        public function __construct($string) {
            $this->name = $string;
        }

    }
?>